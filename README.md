# Build the Project

## Requirements

```bash
Java 11
Maven
Docker
```

## Clone the Repository

```bash
git clone https://gitlab.com/Segun-Ogundipe/drone.git
```

After cloning the repository, navigate into the project

```bash
cd ~/path/to/drone
```

While at the root of the project, run

```bash
docker-compose up -d
```

This command starts the postgres container that is configured for the project.

---

Next run

```bash
./mvnw spring-boot:run
```

This command starts the Spring Boot Application and runs the project at `http://localhost:3000`. To test the endpoints, visit the following routes with postman or your prefered tool

## API ENDPOINTS

| Resource URL | Method  | Description  | Sample Request |
| --- | --- | ----- | ------
| /api/drones| POST | Add a Drone | `{"serialNumber": "dfes43rffe", "model": "LIGHTWEIGHT", "weightLimit": 500, "batteryLevel": 50}` |
| /api/drones/available | GET | Get available drones |
| /api/drones/:drone_serial_number/battery | GET | Get a Drone's battery levels
| /api/medications | POST | Add a medication | `{"name": "Paracetamol","code": "FGHCF54E_4","weight": 5,"imgURL": "http://www.sdfvs.com"}` |
| /api/medications | GET | Get all available medications |
| /api/drones/:drone_serial_number/load | PATCH | Load a drone with medications | `[{"name": "Paracetamol","code": "FGHCF54E_4"}, {"name": "Acetaminophen","code": "HJKLQWHF_4"}]` |
| /api/drones/:drone_serial_number/medications | GET | Get medications loadded on a given drone |

---
