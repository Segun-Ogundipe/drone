package com.segun.drone.persistence.domain.dto;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.segun.drone.persistence.domain.Model;
import com.segun.drone.persistence.domain.State;
import com.segun.drone.persistence.domain.entity.Drone;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DroneDTO {
    @JsonProperty("serialNumber")
    @NotNull
    @NotBlank
    @Size(max = 100)
    private String serialNumber;

    @NotNull
    @JsonProperty("model")
    private Model model;

    @NotNull
    @Max(value = 500)
    @JsonProperty("weightLimit")
    private BigDecimal weightLimit;

    @NotNull
    @Max(value = 100)
    @JsonProperty("batteryLevel")
    private BigDecimal batteryLevel;
    
    @JsonProperty("state")
    private State state;

    @JsonProperty("load")
    private Set<MedicationDTO> medications;

    @JsonProperty("dateCreated")
    private long dateCreated;

    @JsonProperty("dateUpdated")
    private long dateUpdated;

    

    public static DroneDTO fromEntity(Drone drone) {
        return DroneDTO.builder()
            .serialNumber(drone.getSerialNumber())
            .model(drone.getModel())
            .weightLimit(drone.getWeightLimit())
            .batteryLevel(drone.getBatteryLevel())
            .state(drone.getState())
            .medications(Optional.ofNullable(drone.getMedications())
                .orElse(Collections.emptySet())
                .stream()
                .map(MedicationDTO::fromEntity)
                .collect(Collectors.toSet()))
            .dateCreated(drone.getDateCreated())
            .dateUpdated(drone.getDateUpdated())
            .build();
    }

    public static Drone fromDTO(DroneDTO droneDTO) {
        return Drone.builder()
            .serialNumber(droneDTO.getSerialNumber())
            .model(droneDTO.getModel())
            .weightLimit(droneDTO.getWeightLimit())
            .batteryLevel(droneDTO.getBatteryLevel())
            .build();
    }
}