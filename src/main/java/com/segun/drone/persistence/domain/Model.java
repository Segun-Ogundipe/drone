package com.segun.drone.persistence.domain;

public enum Model {
    LIGHTWEIGHT, MIDDLEWEIGHT, CRUISERWEIGHT, HEAVYWEIGHT
}