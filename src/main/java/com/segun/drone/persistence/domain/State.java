package com.segun.drone.persistence.domain;

public enum State {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}