package com.segun.drone.persistence.domain.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.Where;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "medication")
@Where(clause = "deleted=false")
@Getter
@Setter
@NoArgsConstructor
public class Medication {

    @Builder
    public Medication(MedicationID id, BigDecimal weight, String imgURL) {
        this.id = id;
        this.weight = weight;
        this.imgURL = imgURL;
        this.deleted = false;
        this.dateCreated = System.currentTimeMillis();
        this.dateUpdated = System.currentTimeMillis();
    }

    @EmbeddedId
    private MedicationID id;

    @Column(name = "weight", precision = 5, scale = 2)
    @NotNull
    private BigDecimal weight;

    @Column(name = "img_url", nullable = false)
    @NotNull
    @Pattern(regexp = "((http|https)://)(www.)?"
        + "[a-zA-Z0-9@:%._\\+~#?&//=]"
        + "{2,256}\\.[a-z]"
        + "{2,6}\\b([-a-zA-Z0-9@:%"
        + "._\\+~#?&//=]*)")
    private String imgURL;

    @ManyToOne
    @JoinColumn(name = "drone_serial", nullable = true)
    private Drone drone;

    @Column(name = "deleted", nullable = false)
    @NotNull
    private boolean deleted;

    @Column(name = "date_created")
    @NotNull
    private long dateCreated;

    @Column(name = "date_updated")
    @NotNull
    private long dateUpdated;
}