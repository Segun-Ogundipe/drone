package com.segun.drone.persistence.domain.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.segun.drone.persistence.domain.entity.Medication;
import com.segun.drone.persistence.domain.entity.MedicationID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MedicationDTO {
    @NotNull
    @NotBlank
    @Pattern(regexp = "[A-Za-z0-9-_]+")
    private String name;

    @NotNull
    @NotBlank
    @Pattern(regexp = "[A-Z0-9_]+")
    private String code;

    @NotNull
    private BigDecimal weight;

    @Pattern(regexp = "((http|https)://)(www.)?"
        + "[a-zA-Z0-9@:%._\\+~#?&//=]"
        + "{2,256}\\.[a-z]"
        + "{2,6}\\b([-a-zA-Z0-9@:%"
        + "._\\+~#?&//=]*)")
    private String imgURL;

    private long dateCreated;

    private long dateUpdated;
    
    public static MedicationDTO fromEntity(Medication medication) {
        return MedicationDTO.builder()
                .name(medication.getId().getName())
                .weight(medication.getWeight())
                .code(medication.getId().getCode())
                .imgURL(medication.getImgURL())
                .dateCreated(medication.getDateCreated())
                .dateUpdated(medication.getDateUpdated())
                .build();
    }

    public static Medication fromDTO(MedicationDTO medicationDTO) {
        return Medication.builder()
                .id(getMedID(medicationDTO))
                .weight(medicationDTO.getWeight())
                .imgURL(medicationDTO.getImgURL())
                .build();
    }

    public static MedicationID getMedID(MedicationDTO medicationDTO) {
        return MedicationID.builder()
            .code(medicationDTO.getCode())
            .name(medicationDTO.getName())
            .build();
    }
}
