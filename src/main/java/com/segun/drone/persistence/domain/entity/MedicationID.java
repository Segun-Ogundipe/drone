package com.segun.drone.persistence.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MedicationID implements Serializable {
    @Column(name = "name", nullable = false)
    @NotNull
    @NotBlank
    @Pattern(regexp = "[A-Za-z0-9-_]+")
    private String name;

    @Column(name = "code", nullable = false)
    @NotNull
    @NotBlank
    @Pattern(regexp = "[A-Z0-9_]+")
    private String code;
}
