package com.segun.drone.persistence.domain.entity;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.segun.drone.persistence.domain.Model;
import com.segun.drone.persistence.domain.State;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "drone")
@Getter
@Setter
@NoArgsConstructor
public class Drone {

    @Builder
    public Drone(String serialNumber, Model model, BigDecimal weightLimit, BigDecimal batteryLevel) {
        this.serialNumber = serialNumber;
        this.model = model;
        this.weightLimit = weightLimit;
        this.batteryLevel = batteryLevel;
        this.state = State.IDLE;
        this.dateCreated = System.currentTimeMillis();
        this.dateUpdated = System.currentTimeMillis();
    }

    @Id
    @Column(name = "serial_no", length = 100, nullable = false, unique = true)
    @NotNull
    @NotBlank
    @Size(max = 100)
    private String serialNumber;

    @Column(name = "model", nullable = false)
    @Enumerated(EnumType.STRING)
    @NotNull
    private Model model;

    @Column(name = "weight_limit", precision = 5, scale = 2)
    @NotNull
    @Max(value = 500)
    private BigDecimal weightLimit;

    @Column(name = "battery_level", precision = 5, scale = 2)
    @NotNull
    @Max(value = 100)
    private BigDecimal batteryLevel;

    @Column(name = "state", nullable = false)
    @Enumerated(EnumType.STRING)
    @NotNull
    private State state;

    @OneToMany(mappedBy = "drone")
    private Set<Medication> medications;

    @Column(name = "date_created")
    private long dateCreated;

    @Column(name = "date_updated")
    private long dateUpdated;

    public void addMedications(Set<Medication> medications) {
        this.medications.addAll(medications);
    }
}