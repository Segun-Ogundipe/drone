package com.segun.drone.persistence.repository;

import java.math.BigDecimal;
import java.util.Optional;

import com.segun.drone.persistence.domain.State;
import com.segun.drone.persistence.domain.entity.Drone;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface DroneRepository extends CrudRepository<Drone, String> {
    @Query(value = "SELECT d.batteryLevel FROM Drone d WHERE d.serialNumber = :serialNo")
    Optional<BigDecimal> findBatteryLevel(@Param("serialNo") String serialNumber);
    
    Iterable<Drone> findByState(State state);
}