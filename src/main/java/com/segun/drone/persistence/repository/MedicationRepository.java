package com.segun.drone.persistence.repository;

import java.util.Set;

import com.segun.drone.persistence.domain.entity.Medication;
import com.segun.drone.persistence.domain.entity.MedicationID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface MedicationRepository extends CrudRepository<Medication, MedicationID> {
    @Modifying
    @Query(value = "UPDATE Medication SET deleted = true WHERE code = :code AND name = :name")
    void softDeleteMedicine(@Param("code") String code, @Param("name") String name);

    @Query(value = "FROM Medication m WHERE m.drone.serialNumber = :drone_serial")
    Set<Medication> getLoadedMedications(@Param("drone_serial") String droneSerial);
}
