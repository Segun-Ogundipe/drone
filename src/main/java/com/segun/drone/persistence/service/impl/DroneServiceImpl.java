package com.segun.drone.persistence.service.impl;

import java.math.BigDecimal;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.segun.drone.exception.ApiException;
import com.segun.drone.exception.NotFoundException;
import com.segun.drone.persistence.domain.State;
import com.segun.drone.persistence.domain.dto.DroneDTO;
import com.segun.drone.persistence.domain.dto.MedicationDTO;
import com.segun.drone.persistence.domain.entity.Drone;
import com.segun.drone.persistence.domain.entity.Medication;
import com.segun.drone.persistence.repository.DroneRepository;
import com.segun.drone.persistence.repository.MedicationRepository;
import com.segun.drone.persistence.service.DroneService;
import com.segun.drone.persistence.service.MedicationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DroneServiceImpl implements DroneService {
    @Autowired
    private DroneRepository droneRepo;

    @Autowired
    private MedicationRepository medRepo;
    
    @Autowired
    private MedicationService medService;

    @Override
    @Transactional
    public DroneDTO registerDrone(DroneDTO droneDTO) throws ApiException {
        boolean droneExist = droneRepo.findById(droneDTO.getSerialNumber()).isPresent();
        if (droneExist) {
            throw new ApiException("Drone with the provided serial number already exist", HttpStatus.CONFLICT);
        }

        Drone drone = DroneDTO.fromDTO(droneDTO);
        drone = droneRepo.save(drone);
        
        return DroneDTO.fromEntity(drone);
    }

    @Override
    @Transactional(readOnly = true)
    public Double getDroneBatteryLevel(String serialNumber) throws NotFoundException {
        return droneRepo.findBatteryLevel(serialNumber)
            .map(BigDecimal::doubleValue)
            .orElseThrow(() -> new NotFoundException("Drone with the provided serial number does not exist"));
    }

    @Override
    @Transactional(readOnly = true)
    public Set<DroneDTO> getAvailableDrones() {
        return StreamSupport.stream(droneRepo.findByState(State.IDLE).spliterator(), false)
            .map(DroneDTO::fromEntity)
            .collect(Collectors.toSet());
    }

    @Override
    @Transactional
    public void loadDrone(String droneSerialNo, Set<MedicationDTO> medicationDTOs) throws ApiException {
        Drone drone = droneRepo.findById(droneSerialNo).orElseThrow(() -> new NotFoundException("Drone with the provided serial number does not exist"));

        if (drone.getBatteryLevel().doubleValue() < 25.00) {
            throw new ApiException("Drone battery level is below 25%", HttpStatus.BAD_REQUEST);
        }

        drone.setState(State.LOADING);

        Set<MedicationDTO> medDTOs = medService.findAllById(medicationDTOs);

        double weightSum = medDTOs.stream().collect(Collectors.summingDouble(med -> med.getWeight().doubleValue()));

        weightSum += drone.getMedications().stream().collect(Collectors.summingDouble(med -> med.getWeight().doubleValue()));

        if (drone.getWeightLimit().doubleValue() < weightSum) {
            throw new ApiException("The load is greater than the weight limit of the drone", HttpStatus.BAD_REQUEST);
        }

        Set<Medication> medications = medDTOs.stream()
            .map(MedicationDTO::fromDTO)
            .collect(Collectors.toSet());

        for (Medication medication : medications) {
            medication.setDrone(drone);
        }

        medRepo.saveAll(medications);

        drone.setState(State.LOADED);
        // medService.removeAll(medDTOs.stream()
        //     .map(MedicationDTO::getMedID).collect(Collectors.toSet()));

    }

    @Override
    @Transactional(readOnly = true)
    public DroneDTO getDrone(String serialNumber) throws NotFoundException {
        return DroneDTO.fromEntity(droneRepo.findById(serialNumber).orElseThrow(() -> new NotFoundException("Drone with the provided serial number does not exist")));
    }

    @Override
    @Transactional(readOnly = true)
    public Set<DroneDTO> getAllDrones() {
        return StreamSupport.stream(droneRepo.findAll().spliterator(), false)
            .map(DroneDTO::fromEntity)
            .collect(Collectors.toSet());
    }
}