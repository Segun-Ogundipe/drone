package com.segun.drone.persistence.service;

import java.util.Set;

import com.segun.drone.exception.ApiException;
import com.segun.drone.exception.NotFoundException;
import com.segun.drone.persistence.domain.dto.DroneDTO;
import com.segun.drone.persistence.domain.dto.MedicationDTO;

public interface DroneService {
    DroneDTO registerDrone(DroneDTO droneDTO) throws ApiException;
    Double getDroneBatteryLevel(String serialNumber) throws NotFoundException;
    Set<DroneDTO> getAvailableDrones();
    void loadDrone(String droneSerialNo, Set<MedicationDTO> nameValuePair) throws ApiException;
    DroneDTO getDrone(String serialNumber) throws NotFoundException;
    Set<DroneDTO> getAllDrones();
}