package com.segun.drone.persistence.service;

import java.util.Set;

import com.segun.drone.exception.ApiException;
import com.segun.drone.persistence.domain.dto.MedicationDTO;
import com.segun.drone.persistence.domain.entity.MedicationID;

public interface MedicationService {
    MedicationDTO createMedication(MedicationDTO medicationDTO) throws ApiException;
    void remove(MedicationID medID);
    Set<MedicationDTO> getAllMedications();
    void removeAll(Set<MedicationID> medIDs);
    Set<MedicationDTO> findAllById(Set<MedicationDTO> medDTOs);
    Set<MedicationDTO> getLoadedMedications(String droneSerial);
}
