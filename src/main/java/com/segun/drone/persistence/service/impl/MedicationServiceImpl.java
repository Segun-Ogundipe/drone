package com.segun.drone.persistence.service.impl;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.segun.drone.exception.ApiException;
import com.segun.drone.persistence.domain.dto.MedicationDTO;
import com.segun.drone.persistence.domain.entity.Medication;
import com.segun.drone.persistence.domain.entity.MedicationID;
import com.segun.drone.persistence.repository.MedicationRepository;
import com.segun.drone.persistence.service.MedicationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MedicationServiceImpl implements MedicationService {
    @Autowired
    private MedicationRepository medRepo;

    @Override
    @Transactional
    public MedicationDTO createMedication(MedicationDTO medicationDTO) throws ApiException {
        boolean medExist = medRepo.findById(MedicationDTO.getMedID(medicationDTO)).isPresent();

        if (medExist) {
            throw new ApiException("Medication with the provided name/code combination already exist", HttpStatus.CONFLICT);
        }
        

        Medication med = MedicationDTO.fromDTO(medicationDTO);
        med = medRepo.save(med);
        
        return MedicationDTO.fromEntity(med);
    }

    @Override
    @Transactional(readOnly = true)
    public Set<MedicationDTO> findAllById(Set<MedicationDTO> medDTO) {
        return StreamSupport.stream(
            medRepo.findAllById(medDTO.stream()
                .map(MedicationDTO::getMedID)
                .collect(Collectors.toList())).spliterator(), false)
            .map(MedicationDTO::fromEntity)
            .collect(Collectors.toSet());
    }

    @Override
    @Transactional
    public void remove(MedicationID medID) {
        medRepo.softDeleteMedicine(medID.getCode(), medID.getName());
    }

    @Override
    @Transactional
    public void removeAll(Set<MedicationID> medIDs) {
        for (MedicationID medicationID : medIDs) {
            this.remove(medicationID);
        }
    }

    @Override
    @Transactional
    public Set<MedicationDTO> getAllMedications() {
        return StreamSupport.stream(medRepo.findAll().spliterator(), false)
            .map(MedicationDTO::fromEntity)
            .collect(Collectors.toSet());
    }

    @Override
    public Set<MedicationDTO> getLoadedMedications(String droneSerial) {
        return medRepo.getLoadedMedications(droneSerial)
            .stream()
            .map(MedicationDTO::fromEntity)
            .collect(Collectors.toSet());
    }
}