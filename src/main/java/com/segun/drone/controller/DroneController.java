package com.segun.drone.controller;

import java.util.Set;

import javax.validation.Valid;

import com.segun.drone.exception.ApiException;
import com.segun.drone.exception.NotFoundException;
import com.segun.drone.persistence.domain.dto.DroneDTO;
import com.segun.drone.persistence.domain.dto.MedicationDTO;
import com.segun.drone.persistence.service.DroneService;
import com.segun.drone.persistence.service.MedicationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/drones")
public class DroneController {
    @Autowired
    private DroneService droneService;
    @Autowired
    private MedicationService medService;

    @PostMapping(
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<DroneDTO> registerDrone(@RequestBody @Valid DroneDTO droneDTO) throws ApiException {

        DroneDTO drone = droneService.registerDrone(droneDTO);

        return new ResponseEntity<>(drone, HttpStatus.CREATED);
    }

    @GetMapping(
        value = "/available",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Set<DroneDTO>> findAvailableDrones() {
        Set<DroneDTO> availableDrones = droneService.getAvailableDrones();

        return new ResponseEntity<>(availableDrones, HttpStatus.OK);
    }

    @GetMapping(
        value = "/{serialNumber}/battery",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Double> getBatteryLevel(@PathVariable String serialNumber) throws NotFoundException {
        Double batteryLevel = droneService.getDroneBatteryLevel(serialNumber);

        return new ResponseEntity<>(batteryLevel, HttpStatus.OK);
    }

    @PatchMapping(
        value = "/{serialNumber}/load",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<DroneDTO> loadDrone(@PathVariable String serialNumber, @RequestBody @Valid Set<MedicationDTO> medications) throws ApiException {
        droneService.loadDrone(serialNumber, medications);
        DroneDTO drone = droneService.getDrone(serialNumber);

        return new ResponseEntity<>(drone, HttpStatus.OK);
    }

    @GetMapping(
        value="/{serialNumber}/medications"
    )
    public ResponseEntity<Set<MedicationDTO>> getLoadedMedication(@PathVariable String serialNumber) {
        Set<MedicationDTO> loadedMedications = medService.getLoadedMedications(serialNumber);
        
        return new ResponseEntity<>(loadedMedications, HttpStatus.OK);
    }
    
}