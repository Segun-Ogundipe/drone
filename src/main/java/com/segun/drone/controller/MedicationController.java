package com.segun.drone.controller;

import java.util.Set;

import javax.validation.Valid;

import com.segun.drone.exception.ApiException;
import com.segun.drone.persistence.domain.dto.MedicationDTO;
import com.segun.drone.persistence.service.MedicationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/medications")
public class MedicationController {
    @Autowired
    private MedicationService medService;

    @PostMapping(
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<MedicationDTO> addMedication(@RequestBody @Valid MedicationDTO medicationDTO) throws ApiException {
        MedicationDTO med = medService.createMedication(medicationDTO);

        return new ResponseEntity<>(med, HttpStatus.CREATED);
    }

    @GetMapping(
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Set<MedicationDTO>> getAllMedications() {
        Set<MedicationDTO> medications = medService.getAllMedications();

        return new ResponseEntity<>(medications, HttpStatus.OK);
    }
    
}
