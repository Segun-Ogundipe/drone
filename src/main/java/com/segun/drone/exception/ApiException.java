package com.segun.drone.exception;

import java.time.OffsetDateTime;
import java.time.ZoneId;

import org.springframework.http.HttpStatus;

public class ApiException extends Exception {
    private static final long serialVersionUID = 1L;
    private static final ZoneId utc = ZoneId.of("UTC");
    private final OffsetDateTime timestamp;
    private final HttpStatus status;

    public ApiException(HttpStatus status) {
        super();
        this.timestamp = OffsetDateTime.now(utc);
        this.status = status;
    }

    public ApiException(String message, HttpStatus status) {
        super(message);
        this.timestamp = OffsetDateTime.now(utc);
        this.status = status;
    }

    public ApiException(String message, Throwable cause, HttpStatus status) {
        super(message, cause);
        this.timestamp = OffsetDateTime.now(utc);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public OffsetDateTime getTimestamp() {
        return timestamp;
    }
}