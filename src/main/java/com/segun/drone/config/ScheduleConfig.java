package com.segun.drone.config;

import java.util.Set;

import com.segun.drone.persistence.domain.dto.DroneDTO;
import com.segun.drone.persistence.service.DroneService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import lombok.extern.slf4j.Slf4j;

@Configuration
@EnableScheduling
@Slf4j
public class ScheduleConfig {

    @Autowired
    DroneService droneService;

    @Scheduled(cron = "* 0 * * * *")
    public void scheduleCheckBatteryLevels() {

        Set<DroneDTO> drones = droneService.getAllDrones();

        for (DroneDTO droneDTO : drones) {
            log.info("Battery level for {} is: {}%", droneDTO.getSerialNumber(), droneDTO.getBatteryLevel());
        }
    }
}