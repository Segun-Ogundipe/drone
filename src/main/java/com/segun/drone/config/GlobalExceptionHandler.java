package com.segun.drone.config;

import javax.servlet.http.HttpServletRequest;

import com.segun.drone.exception.ApiException;
import com.segun.drone.persistence.domain.dto.ErrorMessage;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<ErrorMessage> handleApiException(ApiException ex, WebRequest request) {
        HttpStatus status = ex.getStatus();
        ErrorMessage errorMessage =
                ErrorMessage.builder()
                        .timestamp(ex.getTimestamp())
                        .status(status.value())
                        .error(status.getReasonPhrase())
                        .message(ex.getMessage())
                        .build();

        if (request instanceof ServletWebRequest) {
            ServletWebRequest servletWebRequest = (ServletWebRequest) request;
            HttpServletRequest servletRequest = servletWebRequest.getNativeRequest(HttpServletRequest.class);
            assert servletRequest != null;
            errorMessage.setPath(servletRequest.getRequestURI());
        }

        return new ResponseEntity<>(errorMessage, status);
    }
}